function makeAlphabetFiller(
  firstLetter: string,
  lastLetter: string,
  debugLabel: string
) {
  const fCode = firstLetter.charCodeAt(0);
  const lCode = lastLetter.charCodeAt(0);
  const alphabetLenght = lCode - fCode;

  return (length: number) => {
    let r = '';
    for (let i = 0; i < length; i++)
      r += String.fromCharCode(fCode + (i % alphabetLenght));

    return r;
  };
}

export const fillAlphabetStr = makeAlphabetFiller('a', 'z', 'fillAlphabetStr');

export const fillRuAlphabetStr = makeAlphabetFiller(
  'а',
  'я',
  'fillRuAlphabetStr'
);

export const fillFromStr = (pattern: string) => (length: number) => {
  let r = '';
  for (let i = 0; i < length; i++) r += pattern.charAt(i % pattern.length);

  return r;
};
