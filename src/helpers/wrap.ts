export type Wrap<T extends Function> = T & { skip: T };

export function Wrap<T extends Function>(
  func: (needSkip?: boolean) => T
): Wrap<T> {
  const r: any = func();
  r.skip = func(true);
  return r;
}
