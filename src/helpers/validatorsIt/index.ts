import {
  ValidateFieldLength,
  validateFieldLengthIt,
} from './validateFieldLengthIt';
import {
  EntityValidateFields,
  validateFieldsLengthIt,
} from './validateFieldsLengthIt';

export namespace ValidatorsIt {
  export namespace FieldLength {
    export type Filler = ValidateFieldLength.Filler;
    export type Definition<TData> = ValidateFieldLength.Definition<TData>;
  }

  export namespace EntityFields {
    export namespace Definition {
      export type Base<TData> = EntityValidateFields.Definition.Base<TData>;
    }

    export type Definition<TData> = EntityValidateFields.Definition<TData>;

    export type Validation = EntityValidateFields.Validation;
  }
}

export const ValidatorsIt = {
  fieldLengthIt: validateFieldLengthIt,
  fieldsLengthIt: validateFieldsLengthIt,
};
