import 'jest';
import type { JSONSchema7 } from 'json-schema';

import { Model } from '@avstantso/node-or-browser-js--model-core';

import {
  ValidateFieldLength,
  validateFieldLengthIt,
} from './validateFieldLengthIt';

export namespace EntityValidateFields {
  export namespace Definition {
    export interface Base<TData> {
      data: TData;
      schema: JSONSchema7;
      fields?: string[];
    }
  }

  export interface Definition<TData> extends Definition.Base<TData> {
    entityType: Model.Types;
  }

  export interface Validation extends Model.FieldInfo {
    field: string;
    filler?: ValidateFieldLength.Filler;
  }
}

export function validateFieldsLengthIt<TDataUnion>(
  describeName: string,
  definitions: EntityValidateFields.Definition<TDataUnion>[],
  validations: EntityValidateFields.Validation[]
) {
  if (!definitions?.length)
    throw new Error(`Empty definitions for "${describeName}"`);

  const filteredDefinitions = definitions.pack();
  if (!filteredDefinitions.length)
    throw new Error(`Empty filtered definitions for "${describeName}"`);

  if (!validations?.length)
    throw new Error(`Empty validations for "${describeName}"`);

  describe(describeName, () =>
    filteredDefinitions.forEach(({ entityType, data, schema, fields }) => {
      const filteredValidations = validations.filter(
        ({ field }) => !fields || fields.includes(field)
      );

      if (!filteredValidations.length)
        throw new Error(
          `Empty filtered validations for "${describeName}${entityType}"`
        );

      describe(`${entityType}`, () =>
        filteredValidations.forEach(({ field, length, pattern, filler }) =>
          validateFieldLengthIt({
            field,
            dataTemplate: data,
            schema,
            maxLength: length?.max,
            minLength: length?.min,
            pattern,
            filler,
          })
        ));
    })
  );
}

validateFieldsLengthIt.Readonly = <T extends Model.Structure>(
  describeName: string,
  mDef: EntityValidateFields.Definition.Base<T['Minimal']>,
  sDef: EntityValidateFields.Definition.Base<T['Select']>,
  ...validations: EntityValidateFields.Validation[]
) =>
  validateFieldsLengthIt<T['Minimal'] | T['Select']>(
    describeName,
    [
      mDef && { entityType: Model.Types.Minimal, ...mDef },
      sDef && { entityType: Model.Types.Select, ...sDef },
    ],
    validations
  );

validateFieldsLengthIt.Simple = <T extends Model.Structure>(
  describeName: string,
  mDef: EntityValidateFields.Definition.Base<T['Minimal']>,
  iDef: EntityValidateFields.Definition.Base<T['Insert']>,
  uDef: EntityValidateFields.Definition.Base<T['Update']>,
  sDef: EntityValidateFields.Definition.Base<T['Select']>,
  ...validations: EntityValidateFields.Validation[]
) =>
  validateFieldsLengthIt<
    T['Minimal'] | T['Insert'] | T['Update'] | T['Select']
  >(
    describeName,
    [
      mDef && { entityType: Model.Types.Minimal, ...mDef },
      iDef && { entityType: Model.Types.Insert, ...iDef },
      uDef && { entityType: Model.Types.Update, ...uDef },
      sDef && { entityType: Model.Types.Select, ...sDef },
    ],
    validations
  );

validateFieldsLengthIt.Hierarchical = <T extends Model.Structure>(
  describeName: string,
  mDef: EntityValidateFields.Definition.Base<T['Minimal']>,
  iDef: EntityValidateFields.Definition.Base<T['Insert']>,
  itDef: EntityValidateFields.Definition.Base<T['InsertTree']>,
  uDef: EntityValidateFields.Definition.Base<T['Update']>,
  utDef: EntityValidateFields.Definition.Base<T['UpdateTree']>,
  sDef: EntityValidateFields.Definition.Base<T['Select']>,
  ...validations: EntityValidateFields.Validation[]
) =>
  validateFieldsLengthIt<
    | T['Minimal']
    | T['Insert']
    | T['InsertTree']
    | T['Update']
    | T['UpdateTree']
    | T['Select']
  >(
    describeName,
    [
      mDef && { entityType: Model.Types.Minimal, ...mDef },
      iDef && { entityType: Model.Types.Insert, ...iDef },
      itDef && { entityType: Model.Types.InsertTree, ...itDef },
      uDef && { entityType: Model.Types.Update, ...uDef },
      utDef && { entityType: Model.Types.UpdateTree, ...utDef },
      sDef && { entityType: Model.Types.Select, ...sDef },
    ],
    validations
  );
