import 'jest';
import type { JSONSchema7 } from 'json-schema';
import type { ErrorObject } from 'ajv';

import { JS } from '@avstantso/node-or-browser-js--utils';
import { validateBySchema } from '@avstantso/node-or-browser-js--model-core';

import { fillAlphabetStr } from '../../utils';
import { ajvExpection } from '../ajvExpections';

export namespace ValidateFieldLength {
  export type Filler = (field: string, lehgth: number, bad?: boolean) => string;

  export interface Definition<TData> {
    field: string;
    dataTemplate: TData;
    schema: JSONSchema7;
    maxLength: number;
    minLength?: number;
    pattern?: string;
    filler?: Filler;
  }
}

export const validateFieldLengthIt = <TData>(
  definition: ValidateFieldLength.Definition<TData>
) => {
  const { field, dataTemplate, schema, maxLength, minLength, pattern, filler } =
    definition;

  describe(field, () => {
    const createTestData = (fieldStrLength: number, bad?: boolean): TData => {
      const data: TData = { ...dataTemplate };

      JS.set.raw(
        data,
        field,
        filler
          ? filler(field, fieldStrLength, bad)
          : fillAlphabetStr(fieldStrLength)
      );

      return data;
    };

    const matchIt = (length?: number) =>
      it('match', () => {
        const data = createTestData(
          JS.is.number(length) ? length : maxLength || 0
        );
        const { isValid, errors } = validateBySchema(data, schema);
        expect({
          ...(!errors
            ? {}
            : {
                errors: 1 === errors.length ? errors[0] : errors,
                field,
                data,
              }),
          isValid,
        }).toStrictEqual({
          isValid: true,
        });
      });

    const ajve = ajvExpection(field);

    const checkFail = (data: TData, expection: ErrorObject) => () => {
      const { isValid, errors } = validateBySchema(data, schema);
      expect(isValid).toBe(false);

      const noSchemaPath = (eoArr: ErrorObject[]) =>
        eoArr.map(({ schemaPath, ...rest }) => rest);

      expect(noSchemaPath(errors)).toStrictEqual(noSchemaPath([expection]));
    };

    if (JS.is.number(minLength))
      describe(`min-length (${minLength})`, () => {
        matchIt(minLength);

        if (!pattern)
          it(
            'fewer',
            checkFail(createTestData(minLength - 1), ajve.minLength(minLength))
          );
      });

    if (JS.is.number(maxLength))
      describe(`max-length (${maxLength})`, () => {
        matchIt();

        if (!pattern)
          it(
            'more',
            checkFail(createTestData(maxLength + 1), ajve.maxLength(maxLength))
          );
      });

    if (pattern)
      describe(`pattern (${pattern})`, () => {
        matchIt(minLength);

        it(
          'mismatch',
          checkFail(createTestData(maxLength, true), ajve.pattern(pattern))
        );
      });
  });
};
