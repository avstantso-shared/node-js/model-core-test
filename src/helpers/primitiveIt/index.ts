import 'jest';

import { Generics } from '@avstantso/node-or-browser-js--utils';
import type { Model } from '@avstantso/node-or-browser-js--model-core';

import { Wrap } from '../wrap';

const testDataObj1 = { x: 1, y: 2, z: 3 };
const testDataObj2 = { i: 8, j: -4, k: 12 };

export namespace PrimitiveIt {
  export type Base = <TPrimitive extends AVStantso.Primitives.Utils<any>>(
    primitive: TPrimitive
  ) => void;
}

function primitiveIt(needSkip?: boolean): PrimitiveIt.Base {
  return (primitive) => {
    const {
      meta: { K: key, L: type },
    }: AVStantso.Primitives.Meta.Provider = Generics.Cast.To(primitive);

    function testDataByType(seria = 0): unknown {
      switch (type) {
        case 'string':
          return !seria ? 'abcc' : 'xyz';
        case 'number':
          return 123 + seria;
        case 'bigint':
          return !seria ? 666666666666666 : 999999999999999;
        case 'boolean':
          return !!seria;
        case 'symbol':
          return Symbol(seria);
        case 'object':
          return !seria ? testDataObj1 : testDataObj2;
        case 'function':
          return !seria ? testDataByType : primitiveIt;

        case 'Date':
          return !seria
            ? new Date()
            : new Date().setHours(new Date().getHours() + seria);

        case 'Buffer':
          return !seria ? Buffer.from('a') : Buffer.from('b');

        default:
          return undefined;
      }
    }
    testDataByType.not = (): unknown => ('string' === type ? 15 : 'xyz');

    function testDataSeria(): [any, any, any] {
      const r: any = [];

      for (let i = 0; i < 3; i++) {
        const data: any = {};
        data[key] = testDataByType(i < 2 ? 0 : i);

        r.push(data);
      }
      return r;
    }

    return (needSkip ? describe.skip : describe)(String(key), () => {
      it('Is', () => {
        const data: any = {},
          other: any = {};
        data[key] = testDataByType();
        other[key] = testDataByType.not();

        expect(primitive.Is(data)).toBe(true);
        expect(primitive.Is(other)).toBe(false);
        expect(primitive.Is(undefined)).toBe(false);
        expect(primitive.Is(null)).toBe(false);
        expect(primitive.Is({})).toBe(false);
        expect(primitive.Is([])).toBe(false);
        expect(primitive.Is(15)).toBe(false);
        expect(primitive.Is('')).toBe(false);
        expect(primitive.Is(false)).toBe(false);
        expect(primitive.Is(testDataByType)).toBe(false);
      });

      it(`Equality`, () => {
        const [data1, data2, data3] = testDataSeria();

        expect(primitive.Equals(data1, data2)).toBe(true);
        expect(primitive.Equals(data1, undefined)).toBe(false);
        expect(primitive.Equals(data1, null)).toBe(false);
        expect(primitive.Equals(data1, {})).toBe(false);
        expect(primitive.Equals(data1, data3)).toBe(false);
      });

      it(`Predicate`, () => {
        const [data1, data2, data3] = testDataSeria();

        const pEQ = primitive.Predicate(data1);
        expect([null, data3, data2, undefined].findIndex(pEQ)).toBe(2);

        const pNEQ = primitive.Predicate(data1, true);
        expect([data2, data3, null, undefined].findIndex(pNEQ)).toBe(1);
      });

      it(`Extract`, () => {
        for (let i = 0; i < 2; i++) {
          const value = testDataByType(i);
          const data: any = {};
          data[key] = value;

          expect(primitive.Extract(data)).toBe(value);
        }
      });

      it(`Compare`, () => {
        const [data1, data2, data3] = testDataSeria();

        expect([data3, data2].sort(primitive.Compare)).toStrictEqual([
          data2,
          data3,
        ]);
      });

      it(`Force`, () => {
        const [data1, data2, data3] = testDataSeria();

        expect(primitive.Force(data1)).toBe(data1);
        expect(primitive.Force(data2)).toBe(data2);
        expect(primitive.Force(data3)).toBe(data3);
      });
    });
  };
}

export type PrimitiveIt = Wrap<PrimitiveIt.Base>;
export const PrimitiveIt = Wrap(primitiveIt);
