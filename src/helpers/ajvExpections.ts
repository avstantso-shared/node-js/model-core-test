export const ajvExpection = (field: string) => ({
  minLength: (minLength: number) => ({
    instancePath: `/${field}`,
    schemaPath: `#/properties/${field}/minLength`,
    keyword: 'minLength',
    params: {
      limit: minLength,
    },
    message: `must NOT have fewer than ${minLength} characters`,
  }),
  maxLength: (maxLength: number) => ({
    instancePath: `/${field}`,
    schemaPath: `#/properties/${field}/maxLength`,
    keyword: 'maxLength',
    params: {
      limit: maxLength,
    },
    message: `must NOT have more than ${maxLength} characters`,
  }),
  pattern: (pattern: string) => ({
    instancePath: `/${field}`,
    schemaPath: `#/properties/${field}/pattern`,
    keyword: 'pattern',
    params: {
      pattern,
    },
    message: `must match pattern "${pattern}"`,
  }),
});
