import type { Model } from '@avstantso/node-or-browser-js--model-core';

export namespace Expections {
  export namespace Readonly {
    export type Get<T extends Model.Structure> = () => Readonly<T>;
  }

  export interface Readonly<T extends Model.Structure> {
    m: T['Minimal'];
  }

  export namespace Simple {
    export type Get<T extends Model.Structure> = () => Simple<T>;
  }

  export interface Simple<T extends Model.Structure> extends Readonly<T> {
    i: T['Insert'];
    u: T['Update'];
  }

  export namespace Hierarchical {
    export type Get<T extends Model.Structure> = () => Hierarchical<T>;
  }

  export interface Hierarchical<T extends Model.Structure> extends Simple<T> {
    it: T['InsertTree'];
    ut: T['UpdateTree'];
  }
}
