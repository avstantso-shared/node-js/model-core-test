import 'jest';
import type { JSONSchema7 } from 'json-schema';

import { Model, cutBySchema } from '@avstantso/node-or-browser-js--model-core';

export function cutter2It<TFrom, TTo>(
  describeName: string,
  data: TFrom,
  schema: JSONSchema7,
  gen: Model.Cutter.Func<TFrom, TTo>,
  expected: TTo
) {
  return describe(describeName, () => {
    let hasError: boolean;
    let actual: TTo;
    let debug: any;
    function noCuttingRequired(): void {
      debug = 'noCuttingRequired';
    }
    function forCutting(errors: any[]): void {
      debug = errors;
    }

    function expectEx(getEntity: () => TTo) {
      try {
        actual = getEntity();
        expect(actual).toStrictEqual(expected);
      } catch (e) {
        hasError = true;
        throw e;
      }
    }

    beforeEach(() => {
      hasError = false;
      actual = undefined;
      debug = undefined;
    });

    afterEach(() => {
      if (!hasError) return;

      const { definitions: dummyDefinitions, ...shortSchema } = schema;
      throw Error(
        JSON.stringify(
          { data, actual, expected, shortSchema, ...(debug ? { debug } : {}) },
          null,
          2
        )
      );
    });

    it('raw', async () => {
      expectEx(() =>
        cutBySchema<TFrom, TTo>(data, schema, {
          debug: { noCuttingRequired, forCutting },
        })
      );
    });

    it('gen', async () => expectEx(() => gen(data)));
  });
}
