import 'jest';

import type { Model } from '@avstantso/node-or-browser-js--model-core';

import { Wrap } from '../wrap';

import { Expections } from './expections';
import { cutter2It } from './cutter2It';

export namespace CutterHierarchicalIt {
  export type Base = <T extends Model.Structure>(
    declaration: Model.Declaration<T>,
    dataI: T['Insert'],
    dataITree: T['InsertTree'],
    dataU: T['Update'],
    dataUTree: T['UpdateTree'],
    dataS: T['Select'],
    expections: Expections.Hierarchical.Get<T>
  ) => void;
}

function cutterHierarchicalIt(needSkip?: boolean): CutterHierarchicalIt.Base {
  return function <T extends Model.Structure>(
    declaration: Model.Declaration<T>,
    dataI: T['Insert'],
    dataITree: T['InsertTree'],
    dataU: T['Update'],
    dataUTree: T['UpdateTree'],
    dataS: T['Select'],
    expections: Expections.Hierarchical.Get<T>
  ) {
    return (needSkip ? describe.skip : describe)(declaration.name, () => {
      const { m, it, i, ut, u } = expections();
      // toMinimal
      cutter2It<T['Select'], T['Minimal']>(
        'S2M',
        dataS,
        declaration.Minimal.Schema,
        declaration.Minimal.Cutter,
        m
      );

      // toInsert
      cutter2It<T['Insert'], T['Insert']>(
        'I2I',
        dataI,
        declaration.Insert.Schema,
        declaration.Insert.Cutter,
        i
      );
      cutter2It<T['InsertTree'], T['Insert']>(
        'ITree2I',
        dataITree,
        declaration.Insert.Schema,
        declaration.Insert.Cutter,
        i
      );
      cutter2It<T['Update'], T['Insert']>(
        'U2I',
        dataU,
        declaration.Insert.Schema,
        declaration.Insert.Cutter,
        i
      );
      cutter2It<T['UpdateTree'], T['Insert']>(
        'UTree2I',
        dataUTree,
        declaration.Insert.Schema,
        declaration.Insert.Cutter,
        i
      );
      cutter2It<T['Select'], T['Insert']>(
        'S2I',
        dataS,
        declaration.Insert.Schema,
        declaration.Insert.Cutter,
        i
      );

      // toInsertTree
      cutter2It<T['UpdateTree'], T['InsertTree']>(
        'UTree2ITree',
        dataUTree,
        declaration.InsertTree.Schema,
        declaration.InsertTree.Cutter,
        it
      );
      cutter2It<T['Select'], T['InsertTree']>(
        'S2ITree',
        dataS,
        declaration.InsertTree.Schema,
        declaration.InsertTree.Cutter,
        it
      );

      // toUpdate
      cutter2It<T['UpdateTree'], T['Update']>(
        'UTree2U',
        dataUTree,
        declaration.Update.Schema,
        declaration.Update.Cutter,
        u
      );
      cutter2It<T['Select'], T['Update']>(
        'S2U',
        dataS,
        declaration.Update.Schema,
        declaration.Update.Cutter,
        u
      );

      // toUpdateTree
      cutter2It<T['Select'], T['UpdateTree']>(
        'S2UTree',
        dataS,
        declaration.UpdateTree.Schema,
        declaration.UpdateTree.Cutter,
        ut
      );
    });
  };
}

export type CutterHierarchicalIt = Wrap<CutterHierarchicalIt.Base>;
export const CutterHierarchicalIt = Wrap(cutterHierarchicalIt);
