import 'jest';

import type { Model } from '@avstantso/node-or-browser-js--model-core';

import { Wrap } from '../wrap';

import { Expections } from './expections';
import { cutter2It } from './cutter2It';

export namespace CutterSimpleIt {
  export type Base = <T extends Model.Structure>(
    declaration: Model.Declaration<T>,
    dataI: T['Insert'],
    dataU: T['Update'],
    dataS: T['Select'],
    expections: Expections.Simple.Get<T>
  ) => void;
}

function cutterSimpleIt(needSkip?: boolean): CutterSimpleIt.Base {
  return function <T extends Model.Structure>(
    declaration: Model.Declaration<T>,
    dataI: T['Insert'],
    dataU: T['Update'],
    dataS: T['Select'],
    expections: Expections.Simple.Get<T>
  ) {
    return (needSkip ? describe.skip : describe)(declaration.name, () => {
      const { m, i, u } = expections();
      // toMinimal
      cutter2It<T['Select'], T['Minimal']>(
        'S2M',
        dataS,
        declaration.Minimal.Schema,
        declaration.Minimal.Cutter,
        m
      );

      // toInsert
      cutter2It<T['Insert'], T['Insert']>(
        'I2I',
        dataI,
        declaration.Insert.Schema,
        declaration.Insert.Cutter,
        i
      );
      cutter2It<T['Update'], T['Insert']>(
        'U2I',
        dataU,
        declaration.Insert.Schema,
        declaration.Insert.Cutter,
        i
      );
      cutter2It<T['Select'], T['Insert']>(
        'S2I',
        dataS,
        declaration.Insert.Schema,
        declaration.Insert.Cutter,
        i
      );

      // toUpdate
      cutter2It<T['Select'], T['Update']>(
        'S2U',
        dataS,
        declaration.Update.Schema,
        declaration.Update.Cutter,
        u
      );
    });
  };
}

export type CutterSimpleIt = Wrap<CutterSimpleIt.Base>;
export const CutterSimpleIt = Wrap(cutterSimpleIt);
