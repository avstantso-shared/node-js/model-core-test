import 'jest';

import type { Model } from '@avstantso/node-or-browser-js--model-core';

import { Wrap } from '../wrap';

import { Expections } from './expections';
import { cutter2It } from './cutter2It';

export namespace CutterReadonlyIt {
  export type Base = <T extends Model.Structure>(
    declaration: Model.Declaration<T>,
    dataS: T['Select'],
    expections: Expections.Readonly.Get<T>
  ) => void;
}

function cutterReadonlyIt(needSkip?: boolean): CutterReadonlyIt.Base {
  return function <T extends Model.Structure>(
    declaration: Model.Declaration<T>,
    dataS: T['Select'],
    expections: Expections.Readonly.Get<T>
  ) {
    return (needSkip ? describe.skip : describe)(declaration.name, () => {
      const { m } = expections();
      // toMinimal
      cutter2It<T['Select'], T['Minimal']>(
        'S2M',
        dataS,
        declaration.Minimal.Schema,
        declaration.Minimal.Cutter,
        m
      );
    });
  };
}

export type CutterReadonlyIt = Wrap<CutterReadonlyIt.Base>;
export const CutterReadonlyIt = Wrap(cutterReadonlyIt);
