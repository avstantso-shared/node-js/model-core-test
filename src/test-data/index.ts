import { v4 as uuidv4 } from 'uuid';
import type { Model } from '@avstantso/node-or-browser-js--model-core';

export const FAKE_UPD: Model.Update = {
  id: uuidv4(),
  version: 0,
};

export const DATES: Model.Dates = {
  putdate: new Date(),
  upddate: new Date(),
};

export function fakeID(): Model.ID {
  return uuidv4();
}

export function fakeUpd<TData extends Model.Insert>(
  data: TData
): TData & Model.Update {
  return { ...FAKE_UPD, ...data };
}

export function fakeDates<TData>(data: TData): TData & Model.Dates {
  return { ...DATES, ...data };
}

export function fakeSel<TData extends Model.Insert>(
  data: TData
): TData & Model.Select {
  return { ...FAKE_UPD, ...DATES, ...data };
}
