import 'jest';
import { Model } from '@avstantso/node-or-browser-js--model-core';

export namespace ExpectEqArrays {
  export interface Overload {
    <T = any>(actual: T[], expected: T[]): void;
    <T = any>(actual: T[], ...expected: T[]): void;
  }
}

export const expectEqArrays: ExpectEqArrays.Overload = <T = any>(
  actual: T[],
  ...params: any[]
) => {
  const expected: T[] =
    1 === params.length && Array.isArray(params[0]) ? params[0] : params;

  const cmp = (a: T, b: T): number => {
    const { id: aid } = Model.IDed.Force(a);
    const { id: bid } = Model.IDed.Force(b);

    if (aid && bid) return aid.localeCompare(bid);

    const { name: an } = Model.Named.Force(a);
    const { name: bn } = Model.Named.Force(b);
    if (an && bn) return an.localeCompare(bn);

    return 0;
  };

  const ac = [...actual];
  const ex = [...expected];

  ac.sort(cmp);
  ex.sort(cmp);

  //console.log('%O\r\n%O', ac, ex);

  expect({ length: ac.length, data: ac }).toStrictEqual({
    length: ex.length,
    data: ex,
  });
};
